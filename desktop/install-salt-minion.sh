#!/bin/sh
# ToDo: convert this to salt-pack. https://github.com/saltstack/salt-pack
# Are you root?
wget -O - https://repo.saltstack.com/py3/debian/10/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/py3/debian/10/amd64/latest buster main" > /etc/apt/sources.list.d/saltstack.list
apt-get update
apt-get -y install salt-minion
# pin minor release
# lay down pre shared keys
echo "master: salt.fionabuckner.xyz" > /etc/salt/minion.d/master
echo $(hostname) | awk '{print tolower($0)}' > /etc/salt/minion_id
systemctl enable salt-minion
systemctl start salt-minion
