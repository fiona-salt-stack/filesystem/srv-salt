base:
  pkg.installed:
    pkgs:
      - vim
      - fonts-firacode
      - tuned
      - wget
      - firefox:
          pkg.installed:
            - fromrepo: deb http://deb.debian.org/debian buster-backports main contrib non-free
