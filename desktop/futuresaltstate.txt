# touch a file that says whether the step has been done or is in progress.
# Install nvidia driver
apt install linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') nvidia-driver
# install sudo and nvidia cuda toolkit
apt-get -y install nvidia-cuda-toolkit \
sudo vim tuned curl \
apt-transport-https \
screen
# mark file as done.
reboot
# mark reboot done
# mark docker install in progress.
# Install docker.
apt-get remove docker docker-engine docker.io containerd runc
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
systemctl enable docker
usermod -aG docker fiona
# nvidia docker
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker

# Build sudoers file

# set tuned
tuned-adm profile network-latency

# install vscode
apt install curl apt-transport-https
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
install -o root -g root -m 644 microsoft.gpg /usr/share/keyrings/microsoft-archive-keyring.gpg
sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-archive-keyring.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
apt-get update
apt-get install code



# post-install cleanup



# lay down dotfiles
# lay down private ssh keys

# Setup borg. https://github.com/hkbakke/saltstack-states/tree/master/borgbackup