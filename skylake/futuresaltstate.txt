sudo apt install ubuntu-drivers-common parallel
sudo ubuntu-drivers autoinstall
sudo apt-get -y remove docker docker-engine docker.io containerd runc
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo usermod -aG docker fiona
sudo reboot
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
sudo apt-get -y install tuned
sudo tuned-adm profile network-latency
mdadm --create \
    --verbose \
    --level=0 \
        --raid-devices=3 \
        --chunk=4608\
    /dev/md/array0 \
    /dev/sda /dev/sdb /dev/sdc
pvcreate /dev/md/array0 &&\
vgcreate array0 /dev/md/array0
sudo lvcreate --size 50G -T -n array0/docker
sudo apt-get -y install xfsdump
sudo mkfs.xfs -b size=4096 -f /dev/mapper/array0-docker


tmpfs /tmp tmpfs nodev,nosuid,size=8G 0 0
tmpfs /tmp/plex tmpfs nodev,nosuid,size=8G 0 0
tmpfs /rear tmpfs nodev,nosuid,size=8G 0 0

192.168.2.15:/volume1/storage /storage nfs vers=3,rsize=1048576,wsize=1048576,timeo=900,retrans=5,_netdev,auto 0 0
pheonix991@tethys.feralhosting.com:/media/sdw1/pheonix991 /feral fuse.sshfs _netdev,users,idmap=user,follow_symlinks,IdentityFile=/root/.ssh/id_ed25519,allow_other,reconnect,default_permissions,uid=10101,gid=10101,Compression=yes,kernel_cache,auto_cache,big_writes,Ciphers=aes256-ctr 0 0

usermod -u 10101 fiona
groupmod -g 10101 fiona
sudo find / -group 1000 -exec chgrp -h fiona {} \;
sudo find / -user 1000 -exec chown -h fiona {} \;




~~~~~~~~
apt install borgbackup
~~~~~~~~~~~~~~
sudo apt install gitlab-runner
W: please run 'sudo /usr/lib/gitlab-runner/mk-prebuilt-images.sh' to generate Docker image.

~~~~~~
Secure docker daemon socket
https://docs.docker.com/engine/security/https/

openssl genrsa -aes256 -out ca-key.pem 4096
fiona@skylake:~/docker-certs$ openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem
Enter pass phrase for ca-key.pem:
Can't load /home/fiona/.rnd into RNG
139875051078080:error:2406F079:random number generator:RAND_load_file:Cannot open file:../crypto/rand/randfile.c:88:Filename=/home/fiona/.rnd
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:Arkansas
Locality Name (eg, city) []:Springdale
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Fiona
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:skylake.fionabuckner.xyz
Email Address []:skylake@cloudwitch.dev

openssl genrsa -out server-key.pem 4096
export HOST=$(hostname)

~~~~~
Make the server able to send mail from a container.
~~~~~
setup that service that blocks connections.